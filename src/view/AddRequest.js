import React, { Component } from 'react'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Editor, EditorState } from 'draft-js'
import { stateToHTML } from 'draft-js-export-html'

const ADD_REQUEST = gql`
  mutation AddRequest($name: String! $text: String! $link: String) {
    addRequest(name: $name text: $text link: $link) {
      id
      name
      isActive
    }
  }
`
const REQUESTS = gql`
query RequestsQuery
{
  requests {
    id
    name
    isActive
  }
}`

class AddRequest extends Component {

  constructor (props) {
    super(props)
    this.state = { text: EditorState.createEmpty(), link: '', name: '' }
    this.onChange = (text) => {
      this.setState({ text })
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value })
  }

  render () {
    let { name, text, link } = this.state
    return (
      <Mutation mutation={ADD_REQUEST}
                update={(cache, { data: { addRequest } }) => {
                  const { requests } = cache.readQuery({ query: REQUESTS })
                  cache.writeQuery({
                    query: REQUESTS,
                    data: { requests: [...requests, addRequest] },
                  })
                  this.props.history.push('/request/' + addRequest.id)
                }}
      >
        {(addRequest, { data }) => (
          <form onSubmit={e => {
            e.preventDefault()
            addRequest({ variables: { name, text: stateToHTML(text.getCurrentContent()), link } })
          }}>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Название</label>
              <input type="text" className="form-control"
                     placeholder="Название" value={name} onChange={this.handleChange('name')}/>
            </div>
            <div className="form-group">
              <label>Тест</label>
              <Editor editorState={text} onChange={this.onChange}/>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Ссылка</label>
              <input type="text" className="form-control"
                     placeholder="Ссылка" value={link} onChange={this.handleChange('link')}/>
            </div>
            <button type="submit" className="btn btn-dark">Сохранить</button>
          </form>)}</Mutation>
    )
  }
}

export default AddRequest