import React, { Component, Fragment } from 'react'

import gql from 'graphql-tag'
import { Query, Mutation } from 'react-apollo'
import { Modal, Button } from 'react-bootstrap'
import { AlertLoad, AlertError, FileInput } from '../components'

let SEND_SV = gql`
mutation SendSV($name:  String! $url:  String! $rate:  Int!){
    sendSV(name : $name url: $url rate: $rate)
}
`

class Request extends Component {
  state = {
    show: false,
    url: '',
    rate: '',
  }

  handleClose = () => this.setState({ show: false })

  handleShow = () => this.setState({ show: true })

  handleUrl = url => this.setState({ url })

  render () {
    let { rate, url } = this.state
    let { match } = this.props
    return (
      <div className={'row'}>
        <div className="col-12 mb-3">
          <Query query={gql`
              query RequestQuery{
                  request (id : "${match.params.id}" ){
                  name
                  text
                  link
                }
              }`
          }>
            {({ loading, error, data }) => {
              if (loading)
                return <AlertLoad/>
              if (error)
                return <AlertError/>

              let { name, text, link } = data.request
              return <Fragment>
                <h3>{name}</h3>
                <p dangerouslySetInnerHTML={{ __html: text }}/>
                <div className={'row'}>
                  <div className={'col-12'}>   {link &&
                  <a className="btn btn-link" href={link} role="button">Подробная информация</a>}</div>
                  <div className={'col-12'}>
                    <button type="button" className="btn btn-dark btn-lg rounded-pill mt-5"
                            onClick={this.handleShow}>Send SV
                    </button>
                    <Modal show={this.state.show} onHide={this.handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>Send SV</Modal.Title>
                      </Modal.Header>

                      <Modal.Body>
                        <Mutation mutation={SEND_SV}>
                          {(sendSV, { data }) => (
                            <form onSubmit={e => {
                              e.preventDefault()
                              if (name && url && rate) {
                                sendSV({ variables: { name, url, rate: parseInt(rate) } })
                                this.setState({ show: false, rate: 0 })
                              }
                            }}>
                              <div className="form-group">
                                <label htmlFor="rate">Rate</label>
                                <input type="number" value={rate}
                                       onChange={e => this.setState({ rate: e.target.value })}
                                       id={'rate'} className="form-control"
                                       placeholder="Rate - $/h"/>
                              </div>
                              <div className="form-group">
                                <FileInput sendUrl={this.handleUrl}/>
                              </div>
                              <Button variant="primary" type={'submit'}>
                                Send SV
                              </Button>
                            </form>)}
                        </Mutation>
                      </Modal.Body>
                    </Modal>
                  </div>
                </div>


              </Fragment>
            }
            }
          </Query>
        </div>
      </div>
    )
  }
}

export default Request
