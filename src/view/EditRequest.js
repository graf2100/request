import React, { Component } from 'react'

import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import { AlertLoad, AlertError } from '../components'

import { Edit } from '../components'

class EditRequest extends Component {
  render () {
    let { match } = this.props
    return (
      <div className={'row'}>
        <div className="col-12 mb-3">
          <Query query={gql`
              query RequestQuery{
                  request (id : "${match.params.id}" ){
                  id
                  name
                  text
                  link
                }
              }`
          }>
            {({ loading, error, data }) => {
              if (loading)
                return <AlertLoad/>
              if (error)
                return <AlertError/>
              return <Edit {...data.request} history={this.props.history}/>
            }
            }
          </Query>
        </div>
      </div>
    )
  }
}

export default EditRequest
