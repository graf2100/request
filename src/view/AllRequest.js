import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

import { TableRequests, AlertError, AlertLoad } from '../components'

const REQUESTS = gql`
query RequestsQuery
{
  requests {
    id
    name
    isActive
  }
}`

function AllRequest () {
  return <Fragment>
    <Link className="btn btn-link mb-2" role="button" to={'/add/request'}>Добавить запрос</Link>
    <Query query={REQUESTS}>
      {
        ({ loading, error, data }) => {
          if (loading)
            return <AlertLoad/>
          if (error)
            return <AlertError/>

          return <TableRequests data={data.requests}/>
        }

      }
    </Query>
  </Fragment>
}

export default AllRequest