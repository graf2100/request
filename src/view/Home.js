import React from 'react'

function Home () {
  return (<div className={'row'}>
    <div className="col-12 mb-3">
      <p className={'text-justify'}>Команда KitRUM стремится привлекать больше компаний-партнеров к проектам,
        которыми делятся наши
        клиенты. Мы создали платформу, где удастся отслеживать актуальные запросы и оперативно на них
        реагировать.</p>
      <p className={'text-justify'}>Мы настроили нотификацию на электронную почту. Поэтому теперь Вы всегда
        будете в курсе поступления
        новых запросов. Как только на сайте будет опубликован новый проект, Вы сразу получите письмо на
        почту!</p>
      <p className={'text-justify'}>Если у Вас в команде есть разработчики с уверенным разговорным английским,
        подходящие под проекты, то
        мы готовы рассмотреть их и преложить клиентам. Для этого поделитесь резюме кандидатов с помощью кнопки
        «Send CV» или присылайте их на почту, в нашу Skype-группу.</p>
      <p className={'text-center mt-3'}>При возникновении вопросов - будем рады ответить.</p>
      <p className={'mt-3'}>Давайте сделаем наше сотрудничество легким и выиграем больше совместных
        проектов в 2019 году!</p>
      <div className={'row mt-5'}>
        <div className={'col-12 col-md-6 mt-3'}>
          <h4>Валентина</h4>
          <dl className="row">
            <dt className="col-sm-3">Skype</dt>
            <dd className="col-sm-9"><a href={'skype:valentina94411?chat'}>valentina94411</a></dd>
            <dt className="col-sm-3">Mail</dt>
            <dd className="col-sm-9"><a href={'mailto:valia@kitrum.com'}>valia@kitrum.com</a></dd>
          </dl>
        </div>
        <div className={'col-12 col-md-6 mt-3'}>
          <h4>Алёна</h4>
          <dl className="row">
            <dt className="col-sm-3">Skype</dt>
            <dd className="col-sm-9"><a href={'skype:alyonasablina?chat'}>alyonasablina</a></dd>
            <dt className="col-sm-3">Mail</dt>
            <dd className="col-sm-9"><a href={'mailto:alyona.honcharenko@kitrum.com'}>alyona.honcharenko@kitrum.com</a>
            </dd>
          </dl>
        </div>
      </div>
    </div>
  </div>)
}

export default Home