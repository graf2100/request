import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Container } from './components'
import { Request, Home, AllRequest, AddRequest,EditRequest  } from './view'
import { ApolloProvider } from 'react-apollo'
import ApolloClient from 'apollo-boost'

let client = new ApolloClient({
  uri: '/graphql'
})

function App () {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Container>
          <Route path="/" exact component={Home}/>
          <Route path="/request/:id" component={Request}/>
          <Route path="/all/request" component={AllRequest}/>
          <Route path="/add/request" component={AddRequest}/>
          <Route path="/edit/request/:id" component={EditRequest}/>
        </Container>
      </Router>
    </ApolloProvider>)
}

export default App
