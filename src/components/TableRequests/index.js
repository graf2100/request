import React from 'react'

import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Link } from 'react-router-dom'
import edit from '../../images/edit.svg'
import trash from '../../images/trash.svg'
import eye_off from '../../images/eye-off.svg'
import eye from '../../images/eye.svg'

let CHANGE_ACTIVE = gql`
mutation ChangeActive($id:  String!){
   changeActive(id: $id){
    id
  }
}
`
let REMOVE_REQUEST = gql`
mutation RemoveRequest($id:  String!){
   removeRequest(id: $id){
    id
  }
}
`
const REQUESTS = gql`
query RequestsQuery
{
  requests {
    id
    name
    isActive
  }
}`

function Item (index, name, isActive, id) {
  return (<tr key={name + index}>
    <th scope="row">{++index}</th>
    <td>{name}</td>
    <td>
      <Mutation mutation={CHANGE_ACTIVE} update={(cache, { data: { changeActive } }) => {
        const { requests } = cache.readQuery({ query: REQUESTS })
        let index = requests.findIndex(request => request.id === changeActive.id)
        if (index !== -1) {
          requests[index].isActive = !requests[index].isActive
          cache.writeQuery({
            query: REQUESTS,
            data: { requests: [...requests] },
          })
        }
      }}>
        {(changeActive, { data }) => (
          <button type="button" className="btn btn-link" onClick={() => changeActive({ variables: { id } })}><img
            className={'w-75'}
            src={isActive ? eye : eye_off}
            alt={'eye'}/></button>
        )}</Mutation>

      <Link  to={'/edit/request/' + id} className="btn btn-link"> <img
        className={'w-75'} src={edit} alt={'edit'}/></Link>


      <Mutation mutation={REMOVE_REQUEST} update={(cache, { data: { removeRequest } }) => {
        const { requests } = cache.readQuery({ query: REQUESTS })
        let index = requests.findIndex(request => request.id === removeRequest.id)
        if (index !== -1) {
          requests.splice(index, 1)
          cache.writeQuery({
            query: REQUESTS,
            data: { requests: [...requests] },
          })
        }
      }}>
        {(removeRequest, { data }) => (
          <button type="button" onClick={() => removeRequest({ variables: { id } })} className="btn btn-link"><img
            className={'w-75'} src={trash} alt={'remove'}/></button>

        )}</Mutation>


    </td>
  </tr>)
}

function TableRequests (props) {
  let { data } = props
  return (
    <table className="table table-borderless">
      <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Название</th>
        <th scope="col"/>
      </tr>
      </thead>
      <tbody>
      {data.map(({ id, name, isActive }, index) => Item(index, name, isActive, id))}
      </tbody>
    </table>)
}

export default TableRequests