import React from 'react'

function AlertError () {
  return (
    <div className="alert alert-danger" role="alert">
      Ошибка!
    </div>
  )
}

export default AlertError