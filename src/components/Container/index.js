import React from 'react'
import { TopBar } from '../index'
import banner from '../../images/3.png'

function Container (props) {
  return (
    <div className="App">
      <TopBar/>
      <img src={banner} className="img-fluid mt-md-n5 w-100" alt="Responsive"/>
      <div className="container rounded-top p-5 mt-md-n5 mb-5 mt-sm-n3 shadow-lg">
        {props.children}
      </div>
    </div>)
}

export default Container