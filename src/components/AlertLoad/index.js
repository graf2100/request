import React from 'react'

function AlertLoad () {
  return (
    <div className="alert alert-dark" role="alert">
      Загрузка!
    </div>
  )
}

export default AlertLoad