import React, { Component, Fragment } from 'react'
import * as firebase from 'firebase'


let config = {
  apiKey: 'AIzaSyA7HZx450aIJolxP-xxGDrrM3E6naQnBxI',
  authDomain: 'request-3748c.firebaseapp.com',
  databaseURL: 'https://request-3748c.firebaseio.com',
  projectId: 'request-3748c',
  storageBucket: 'request-3748c.appspot.com',
  messagingSenderId: '154943538296'
}

firebase.initializeApp(config)

class FileInput extends Component {
  state = {
    isLoad: false,
    isComplete: false,
    progress: 0,
  }

  upload = (e) => {
    let file = e.target.files[0]
    let ref = firebase.storage().ref('files/' + file.name)
    let task = ref.put(file)

    task.on('state_changed', snapshot => {
      let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
      this.setState({ progress, isLoad: true })
    }, err => {console.log(err)}, () => {
      task.snapshot.ref.getDownloadURL().then(async url => {
        this.props.sendUrl(url)
        this.setState({ isComplete: true, isLoad: false })
      })

    })
  }

  render () {
    let { isLoad, progress, isComplete } = this.state
    return (<Fragment>
      <label style={{
        padding:'0.7rem 1.2rem'
      }} className={isComplete ? 'border rounded border-success text-success' : 'border rounded border-primary text-primary'}>
        {isLoad ? progress + ' %' : isComplete ? 'Uploading' : 'Add file SV'}
        <input accept="image/png, image/jpeg" type={'file'}
               className={'invisible position-absolute'} onChange={this.upload}/>
      </label>
    </Fragment>)
  }
}

export default FileInput