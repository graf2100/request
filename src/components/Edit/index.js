import React, { Component } from 'react'
import { stateToHTML } from 'draft-js-export-html'
import { Editor, EditorState } from 'draft-js'
import { stateFromHTML } from 'draft-js-import-html'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

const EDIT_REQUEST = gql`
  mutation EditRequest($id:String! $name: String! $text: String! $link: String) {
    editRequest(id: $id name: $name text: $text link: $link) {
      id
      name
      link
      text
      isActive
    }
  }
`
const REQUESTS = gql`
query RequestsQuery
{
  requests {
    id
    name
    isActive
  }
}`

const REQUEST = id => gql`query RequestQuery{
                            request (id : "${id}" ){
                            name
                            text
                            link
                          }
                        }`

class Edit extends Component {

  constructor (props) {
    super(props)
    this.state = { text: EditorState.createWithContent(stateFromHTML(props.text)), link: props.link, name: props.name }
    this.onChange = (text) => {
      this.setState({ text })
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value })
  }

  render () {
    let { name, text, link } = this.state
    let { id } = this.props
    return (
      <Mutation mutation={EDIT_REQUEST}
                update={(cache, { data: { editRequest } }) => {
                  const { requests } = cache.readQuery({ query: REQUESTS })
                  const { request } = cache.readQuery({
                    query: REQUEST(editRequest.id)
                  })
                  let index = requests.findIndex(request => request.id === editRequest.id)
                  if (index !== -1) {
                    requests.splice(index, 1, editRequest)
                    request.name = editRequest.name
                    request.text = editRequest.text
                    request.link = editRequest.link
                    cache.writeQuery({
                      query: REQUESTS,
                      data: { requests: [...requests] },
                    })
                    cache.writeQuery({
                      query: REQUEST(editRequest.id),
                      data: { ...request },
                    })
                    this.props.history.push('/request/' + editRequest.id)
                  }
                }}
      >
        {(editRequest, { data }) => (
          <form onSubmit={e => {
            e.preventDefault()
            editRequest({ variables: { id, name, text: stateToHTML(text.getCurrentContent()), link } })
          }}>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Название</label>
              <input type="text" className="form-control"
                     placeholder="Название" value={name} onChange={this.handleChange('name')}/>
            </div>
            <div className="form-group">
              <label>Тест</label>
              <Editor editorState={text} onChange={this.onChange}/>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Ссылка</label>
              <input type="text" className="form-control"
                     placeholder="Ссылка" value={link} onChange={this.handleChange('link')}/>
            </div>
            <button type="submit" className="btn btn-dark">Сохранить</button>
          </form>)}</Mutation>
    )
  }
}

export default Edit