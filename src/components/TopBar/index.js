import React from 'react'

import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from '../../images/kitrum-logo-website.png'

import gql from 'graphql-tag'
import { Query } from 'react-apollo'

const REQUESTS = gql`
query RequestsQuery
{
  requests {
    id
    name
    isActive
  }
}`

const TopBar = () => (
  <Navbar fixed="top" bg="transparent" expand="lg">
    <Navbar.Brand href="#home"> <img src={logo} width={75} className="d-inline-block align-top"
                                     alt="Logo"/></Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
    <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
      <Nav className="float-right">
        <Link className={'nav-link'} role="button" to={'/'}>Home</Link>
        <Query query={REQUESTS}>
          {
            ({ loading, error, data }) => {
              if (loading)
                return ''
              if (error)
                return ''

              return data.requests.filter(request => request.isActive === true).map(request => <Link key={request.id}
                                                                                                     className={'nav-link'}
                                                                                                     role="button"
                                                                                                     to={'/request/' + request.id}>{request.name}</Link>)
            }

          }
        </Query>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
)

export default TopBar