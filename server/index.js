const express = require('express')
const path = require('path')
const graphqlHTTP = require('express-graphql')
const cors = require('cors')

const schema = require('./schema')

const app = express()

/*app.use(express.static(path.join(__dirname, '..', 'build')))

app.get('*', (req, res) => {
  res.sendFile(path.join(path.join(__dirname, '..', 'build', 'index.html')))
})*/
app.use(cors())
app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}))

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log(`Start server on ${PORT}`))