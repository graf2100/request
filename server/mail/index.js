const nodemailer = require('nodemailer')

let AUTH = {
  service: 'gmail',
  auth: {
    user: 'graf2100@gmail.com',
    pass: 'j3eZ7VNkRsvinp4'
  }
}

async function sendSV (name, rate, url) {

  let transporter = nodemailer.createTransport(AUTH)

  let mailOptions = {
    from: `"SV helper 👻" <${AUTH.auth.user}>`,
    to: 'alyona.honcharenko@kitrum.com,valia@kitrum.com',
    subject: 'SV for' + name,
    html: `<b>${name}<br/>
Rate - ${rate} $/h
<br/>
<a href="${url}">Open file</a>
</b>`,
    attachments: {
      path: url
    }
  }

  let info = await transporter.sendMail(mailOptions)
  if (info) {
    return 'Done'
  } else {
    return 'Fail'
  }
}

async function sendReq (name, to) {

  let transporter = nodemailer.createTransport(AUTH)
  let mailOptions = {
    from: AUTH.auth.user,
    to,
    subject: 'New request ' + name,
    html: `<div>
    <table
            style="font-size:17px;line-height:24px;color:#373737;background:#f9f9f9;width:100%;border-collapse:collapse;border:0">
        <tbody>
        <tr>
            <td style="vertical-align:top;padding:0">
                <table
                        style="width:100%;border-collapse:collapse;border:0;margin:0 auto">
                    <tbody>
                    <tr>
                        <td style="padding:20px 16px 12px;vertical-align:bottom">
                            <div style="text-align:center"></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;padding:0">
                <center>
                    <table style="border:0;border-collapse:collapse;margin:0 auto;background:white;border-radius:8px;margin-bottom:16px">
                        <tbody>
                        <tr>
                            <td style="width:546px;vertical-align:top;padding:32px">
                                <div style="max-width:600px;margin:0 auto"><h2
                                        style="color:#2ab27b;margin:0 0 12px;line-height:30px">Здравствуйте!</h2>
                                    <p>Спешим сообщить, что добавили новый проект ${name}.</p>
                                    <p>Скорее ознакомьтесь с описанием и присылайте резюме кандидатов.</p>
                                    <img style="margin-top: 17px" src="https://firebasestorage.googleapis.com/v0/b/request-3748c.appspot.com/o/%D0%B6%D0%B4%D1%83%D0%BD.png?alt=media&token=6d56482a-8191-4971-a8bf-7790063e17d2"/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td style="padding:0">
            </td>
        </tr>
        </tbody>
    </table>
</div>`
  }

  let info = await transporter.sendMail(mailOptions)
  if (info) {
    return 'Done'
  } else {
    return 'Fail'
  }
}

module.exports = { sendSV, sendReq }