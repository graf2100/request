const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean, GraphQLList, GraphQLSchema, } = require('graphql')
const { Request } = require('../db')
const { sendSV } = require('../mail')
const { emitter } = require('../event')
const RequestType = new GraphQLObjectType({
  name: 'Request',
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: GraphQLString },
    text: { type: GraphQLString },
    link: { type: GraphQLString },
    isActive: { type: GraphQLBoolean },
  })
})

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    requests: {
      type: new GraphQLList(RequestType),
      resolve (parent, args) {
        return Request.find({})
      }
    },
    request: {
      type: RequestType,
      args: {
        id: { type: GraphQLString },
      },
      async resolve (parent, args) {
        let { id } = args
        return Request.findOne({ _id: id })
      }
    }
  }
})

const RootMutation = new GraphQLObjectType({
  name: 'RootMutationType',
  fields: {
    addRequest: {
      type: RequestType,
      args: {
        name: { type: GraphQLString },
        text: { type: GraphQLString },
        link: { type: GraphQLString },
      },
      async resolve (parent, args) {
        let { name, text, link } = args
        if (name && text) {
          emitter.emit('mailing', name)
          return await new Request({ name, text, link }).save()
        }
      }
    },
    removeRequest: {
      type: RequestType,
      args: {
        id: { type: GraphQLString },
      },
      async resolve (parent, args) {
        let { id } = args
        let req = await Request.findOne({ _id: id })
        await Request.deleteOne({ _id: id })
        return req
      }
    },
    editRequest: {
      type: RequestType,
      args: {
        name: { type: GraphQLString },
        id: { type: GraphQLString },
        text: { type: GraphQLString },
        link: { type: GraphQLString },
      },
      async resolve (parent, args) {
        let { id, name, text, link } = args
        if (id && name && text) {
          let req = await Request.updateOne({ _id: id }, { name, text, link })
          if (req) {
            req = await Request.findOne({ _id: id })
            return req
          }
        }
      }
    },
    changeActive: {
      type: RequestType,
      args: {
        id: { type: GraphQLString }
      },
      async resolve (parent, args) {
        let { id } = args
        let req = await Request.findOne({ _id: id })
        req.isActive = !req.isActive
        await req.save()
        return req
      }
    },
    sendSV: {
      type: GraphQLString,
      args: {
        name: { type: GraphQLString },
        rate: { type: GraphQLInt },
        url: { type: GraphQLString }
      },
      async resolve (parent, args) {
        let { name, rate, url } = args
        if (name && rate && url) {
          return await sendSV(name, rate, url)
        } else {
          return 'Fail'
        }
      }
    }
  }
})

module.exports = new GraphQLSchema({ query: RootQuery, mutation: RootMutation })
