const needle = require('needle')
const cheerio = require('cheerio')
const EventEmitter = require('events')
const { sendReq } = require('../mail')
let URL = 'https://script.google.com/macros/s/AKfycbw5FqSkhkgaE3JE3rjjZ1U4rCmsOlik4K_qKUxcFDRQiviInuU/exec'

async function getMails () {
  const res = await needle('get', URL)
  const $ = await cheerio.load(res.body)
  const sheets = await needle('get', $('a').attr('href'))
  let mails = JSON.parse(sheets.body)
  let str = ''
  mails.forEach((mail, index) => {
    if (index === 0) {
      str += mail[0]
    } else {
      str += ',' + mail[0]
    }
  })
  return str
}

class Emitter extends EventEmitter {}

const emitter = new Emitter()
emitter.on('mailing', async (name) => {
  let mails = await getMails()
  await sendReq(name, mails)
})
module.exports = { emitter }
