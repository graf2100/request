const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect('mongodb://root:21002100q@ds149914.mlab.com:49914/requests', { useNewUrlParser: true })

const requestSchema = new Schema({
  name: String,
  text: String,
  link: { type: String, default: '' },
  isActive: { type: Boolean, default: true },
})

const Request = mongoose.model('request', requestSchema)

module.exports = { Request }